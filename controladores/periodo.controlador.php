<?php

class ControladorPeriodo{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$periodos = ModeloPeriodo::index("periodo");

		$json = array(

			"status"=>200,
			"total_registros"=>count($periodos),
			"detalle"=>$periodos
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Registrar un nuevo periodo
	=============================================*/
	public function create($datos){


		/*=============================================
		Validar que el periodo no este repetido
		=============================================*/
		$periodo = ModeloPeriodo::index("periodo");

		foreach ($periodo as $key => $value) {
			
			if($value->anio == $datos["anio"]){
				$json = array(

				"status"=>300,
				"detalle"=>"El periodo ".$datos["anio"]." ya esta registrado"

				);

				echo json_encode($json, true);

				return;		
			}
		}


		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloPeriodo::create("periodo",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"El el periodo se ha registrado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	
	static public function update($id, $datos){
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$update = ModeloPeriodo::update("periodo", $id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($update == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"El periodo se ha modificado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}
	}

	static public function delete($valor){

		/*=============================================
		Verificar que no hay matriculas registradas
		=============================================*/
		$veri = ModeloMatricula::index("v_listadoalumnos");

		foreach ($veri as $key => $value) {
			if($valor == $value->id_periodo){

				$json = array(
	    		 	"status"=>300,
	    			"detalle"=>"Hay matriculas registradas en este periodo, no se puede borrar."

		    	);
	    	
	    		echo json_encode($json, true); 

	    		return;
			}
		}

		$borrar = ModeloPeriodo::delete("periodo", $valor);

		if($borrar == "ok"){
			$json = array(
    		 	"status"=>200,
    			"detalle"=>"Se ha borrado el periodo con éxito"

	    	);
    	
    		echo json_encode($json, true); 

    		return;
    	}
	}
}