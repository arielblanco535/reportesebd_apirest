<?php

class ControladorOfrenda{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$ofrendas = ModeloOfrenda::index("ofrenda");

		$json = array(

			"status"=>200,
			"total_registros"=>count($ofrendas),
			"detalle"=>$ofrendas
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Registrar una nueva ofrenda
	=============================================*/
	public function create($datos){
		/*=============================================
		Validar que La fecha no este repetida
		=============================================*/
		$list = ModeloOfrenda::index("ofrenda");

		$enc = ModeloEncuentro::index("encuentro");
		$ok = "";
		foreach ($enc as $key => $value) {
			if($value->fecha == $datos["fecha"]){
				$ok = "ok";
				break;
			}
		}
		

		if($ok == ""){
			$json = array(

			"status"=>400,
			"detalle"=>"La fecha ".$datos["fecha"]." ya esta registrado para esta clase"

			);

			echo json_encode($json, true);

			return;		

		}

		foreach ($list as $key => $value) {
			
			if($value->fecha_recaudado == $datos["fecha"] && $value->id_clase == $datos["idClase"]){
				$json = array(

				"status"=>300,
				"detalle"=>"La fecha ".$datos["fecha"]." ya esta registrado para esta clase"

				);

				echo json_encode($json, true);

				return;		
			}
		}
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloOfrenda::create("ofrenda",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"La ofrenda se ha registrado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	/*=============================================
	Mostrar un solo encuentro
	=============================================*/
	public function show($id){
		/*=============================================
		Mostrar todos los encuentros
		=============================================*/
		$item = ModeloOfrenda::showClase("ofrenda", $id);

		if(!empty($item)){

			$json = array(

				"status"=>200,
				"total_registros"=>count($item),
				"detalle"=>$item

			);

			echo json_encode($json, true);

			return;

		}else{

			$json = array(

	    		"status"=>300,
	    		"total_registros"=>0,
	    		"detalles"=>"No hay ningún encuentro registrado"
	    		
	    	);

			echo json_encode($json, true);	

			return;

		}
	}


	static public function update($id, $datos){
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$edit = ModeloOfrenda::update("ofrenda", $id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($edit == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"La ofrenda se ha modificado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		} else {
			$json = array(

				"status"=>300,
				"detalle"=>"Error al modificar la ofrenda",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}
	}

	static public function delete($id){

		$borrar = ModeloOfrenda::delete("ofrenda", $id);

		if($borrar == "ok"){
			$json = array(
    	 	"status"=>200,
    		"detalle"=>"Se ha borrado la ofrenda con éxito"

    		);
    	
    		echo json_encode($json, true); 

    		return;
	    } else {
			$json = array(
    	 	"status"=>300,
    		"detalle"=>"Error al borrar la ofrenda"

    		);
    	
    		echo json_encode($json, true); 

    		return;

	    }
	}
}