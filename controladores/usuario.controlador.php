<?php

class ControladorUsuario{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$usuarios = ModeloUsuario::index(1,"usuarios");

		$json = array(

			"status"=>200,
			"total_registros"=>count($usuarios),
			"detalle"=>$usuarios
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Registrar un nuevo Usuario
	=============================================*/
	public function create($datos){

		/*=============================================
		Validar nombre
		=============================================*/

		if(isset($datos["nombre"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["nombre"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo nombre, solo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}

		/*=============================================
		Validar que el nombre no este repetido
		=============================================*/
		$clases = ModeloUsuario::index(2,"usuarios");


		foreach ($clases as $key => $value) {
			
			if($value["usuario"] == $datos["usuario"]){
				$json = array(

				"status"=>404,
				"detalle"=>"El usuario ".$datos["usuario"]." ya esta registrado"

				);

				echo json_encode($json, true);

				return;		
			}

		}


		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloUsuario::create("usuarios",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>404,
				"detalle"=>"El usuario se ha registrado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	
	public function actualizar($item1, $valor1, $item2, $valor2){
		$tabla = "usuarios";

		$respuesta = ModeloUsuario::actualizar($tabla, $item1, $valor1, $item2, $valor2);

		if($respuesta == "ok"){
			$json = array(

				"status"=>404,
				"result"=>"ok",
				"detalle"=>"El estado del usuario ha sido actualizado"

				);

				echo json_encode($json, true);

				return;		
		}

	}
}