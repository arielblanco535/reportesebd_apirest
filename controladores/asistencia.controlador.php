<?php 

class ControladorAsistencia{

	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$asistencia = ModeloAsistencia::index("v_asistenciadominical5");

		$json = array(

			"status"=>200,
			"total_registros"=>count($asistencia),
			"detalle"=>$asistencia
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Mostrar un solo registro
	=============================================*/
	public function show($id){
		$matricula = ModeloMatricula::show("v_listadoalumnos", "id_matricula", $id);
		$mat = ModeloMatricula::show("matricula", "id_matricula", $id);
		$fecha = "";
		foreach ($mat as $key => $value) {
			$fecha=$value->fecha;
			break;
		}
		foreach ($matricula as $key => $value) {
				
			$json = array(

				"status"=>200,
				"total_registros"=>count($matricula),
				"matricula"=>$value->id_matricula,
				"codigo"=>$value->codigo_alumno,
				"nombre"=>$value->nombre_alumno,
				"apellido"=>$value->apellido_alumno,
				"fechaNac"=>$value->fecha_nacimiento,
				"fecha"=>$fecha,
				"clase"=>$value->id_clase,
				"estado"=>$value->estado,
				"anio"=>$value->anio
			);

			echo json_encode($json, true);

			return;
		}
	}

	/*=============================================
	Crear nueva matricula
	=============================================*/
	public function create($datos){

		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloAsistencia::create("asistencia",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"la asistencia se ha registrado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	/*=============================================
	Editar nueva matricula
	=============================================*/
	public function update($id, $datos){

		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$update = ModeloAsistencia::update("asistencia",$id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($update == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"la asistencia se ha modificado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

}
