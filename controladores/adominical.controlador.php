<?php

class ControladorADominical{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index($clase, $fecha){
		$dominical = ModeloADominical::index(1,$clase, $fecha);
		$dominical2 = ModeloADominical::index(2,$clase, $fecha);

		$presente = 0;
		$ausente = 0;
		foreach ($dominical2 as $key => $value) {
			if($value["presente"] == 1){
				$presente = $presente + 1;
			}else{
				$ausente = $ausente + 1;
			}
		}
		$json = array(

			"status"=>200,
			"presentes"=>$presente,
			"ausentes"=>$ausente,
			"total_registros"=>count($dominical),
			"detalle"=>$dominical
		);

		
		echo json_encode($json, true);

		return;
	}

}