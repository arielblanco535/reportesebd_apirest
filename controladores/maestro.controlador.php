<?php

class ControladorMaestro{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$maestros = ModeloMaestro::index("maestro");

		$json = array(

			"status"=>200,
			"total_registros"=>count($maestros),
			"detalle"=>$maestros
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Registrar un nuevo maestro
	=============================================*/
	static public function create($datos){
		/*=============================================
		Validar nombre
		=============================================*/

		if(isset($datos["nombre"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["nombre"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo nombre, sólo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}

		if(isset($datos["apellido"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["apellido"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo apellido, sólo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}

		/*=============================================
		Validar que el nombre no este repetido
		=============================================*/
		$maestro = ModeloMaestro::index("maestro");

		foreach ($maestro as $key => $value) {
			
			if($value->nombre_maestro." ".$value->apellido_maestro == $datos["nombre"]." ".$datos["apellido"]){
				$json = array(

				"status"=>404,
				"detalle"=>"El alumno ".$datos["nombre"]." ".$datos["apellido"]." ya esta registrado"

				);

				echo json_encode($json, true);

				return;		
			}
		}

		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloMaestro::create("maestro",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>404,
				"detalle"=>"El maestro se ha registrado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	public function actualizar($datos){
		$tabla = "maestro";

		if($datos["item1"] == "estado"){

			$estado = ModeloMaestro::show("maestro", $datos["valor2"]);
			foreach ($estado as $key => $value) {
				if($datos["valor1"] == 0){
					if($value->titular == 1){

						$json = array(

							"status"=>300,
							"result"=>"stop",
							"detalle"=>"No se puede desactivar un maestro titular"

							);

						echo json_encode($json, true);
						return;		
					}
				}
			}

			$respuesta = ModeloMaestro::actualizar($tabla, $datos["item1"], $datos["valor1"], $datos["item2"], $datos["valor2"]);

			if($respuesta == "ok"){
				$json = array(

					"status"=>200,
					"result"=>"ok",
					"detalle"=>"El estado del maestro ha sido actualizado"

					);

					echo json_encode($json, true);

					return;		
			}
		} else {
			/*=============================================
			Comprobar el estado del maestro
			=============================================*/
			$estado = ModeloMaestro::show("maestro", $datos["valor2"]);

			foreach ($estado as $key => $value) {
				if($value->estado == 0){

					$json = array(

						"status"=>300,
						"result"=>"error",
						"detalle"=>"El maestro titular no puede estar inactivo"

					);

					echo json_encode($json, true);

					return;		
				}
			}

			/*=============================================
			Comprobar que no hay otro maestro titular de la clase
			=============================================*/
			$titular = ModeloMaestro::index("maestro", $datos["valor2"]);
			foreach ($titular as $key => $value) {
				if($datos["valor1"] == 1){
					if($value->id_clase == $datos["idClase"] && $value->titular == 1){
						$json = array(

							"status"=>400,
							"result"=>"stop",
							"detalle"=>"La clase ya tiene un maestro titular"

						);

						echo json_encode($json, true);

						return;		
					}
				}
			}

			/*=============================================
			Llevar datos al modelo
			=============================================*/
			$respuesta = ModeloMaestro::actualizar($tabla, $datos["item1"], $datos["valor1"], $datos["item2"], $datos["valor2"]);

			/*=============================================
			Respuesta del modelo
			=============================================*/
			if($respuesta == "ok"){
				$json = array(

					"status"=>200,
					"result"=>"ok",
					"detalle"=>"El maestro ha sido actualizado"

					);

				echo json_encode($json, true);

				return;		
			}
		}
	}

	static public function update($id, $datos){
		/*=============================================
		Validar nombre
		=============================================*/

		if(isset($datos["nombre"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["nombre"])||
			isset($datos["apellido"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["apellido"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo nombre y apellido, solo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$edit = ModeloMaestro::update("maestro", $id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($edit == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"El maestro se ha modificado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}
	}

	static public function delete($valor){

		/*=============================================
		Verificar que el alumno no sea titular
		=============================================*/
		$lista = ModeloMaestro::show("maestro", $valor);

		foreach ($lista as $key => $value) {
			if($value->titular == 1){
				$json = array(

		    		"status"=>300,
		    		"detalles"=>"El maestro es titular, no se puede eliminar"
		    		
		    	);

				echo json_encode($json, true);	

				return;

			}
		}

		$borrar = ModeloMaestro::delete("maestro", $valor);

		if($borrar == "ok"){
			$json = array(
    		 	"status"=>200,
    			"detalle"=>"Se ha borrado al maestro con éxito"

	    	);
	    	
    		echo json_encode($json, true); 

    		return;
    	}
		
	}
}