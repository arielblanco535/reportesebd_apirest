<?php

class ControladorClase{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$clases = ModeloClase::index(1,"clase");

		$json = array(

			"status"=>200,
			"total_registros"=>count($clases),
			"detalle"=>$clases
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Registrar una nueva clase
	=============================================*/
	public function create($datos){

		/*=============================================
		Validar nombre
		=============================================*/

		if(isset($datos["nombre"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ -]+$/', $datos["nombre"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo nombre, solo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}

		/*=============================================
		Validar que el nombre no este repetido
		=============================================*/
		$clases = ModeloClase::index(2,"clase");

		foreach ($clases as $key => $value) {
			
			if($value["nombre"] == $datos["nombre"]){
				$json = array(

				"status"=>300,
				"detalle"=>"El nombre ".$datos["nombre"]." ya esta registrado"

				);

				echo json_encode($json, true);

				return;		
			}
		}


		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloClase::create("clase",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"El clase se ha registrado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	
	static public function update($id, $datos){
		/*=============================================
		Validar nombre
		=============================================*/
		if(isset($datos["nombre"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ -]+$/', $datos["nombre"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo nombre, solo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$edit = ModeloClase::update("clase", $id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($edit == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"La clase se ha modificado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}
	}

	static public function delete($valor){
		/*=============================================
		Verificar que no hay matriculas registradas
		=============================================*/
		$veri = ModeloMatricula::index("v_listadoalumnos");

		foreach ($veri as $key => $value) {
			if($valor == $value->id_clase){

				$json = array(
	    		 	"status"=>300,
	    			"detalle"=>"Hay matriculas registradas en esta clase, no se puede borrar."

		    	);
	    	
	    		echo json_encode($json, true); 

	    		return;
			}
		}
		
		$borrar = ModeloClase::delete("clase", $valor);

		if($borrar == "ok"){
			$json = array(
    		 	"status"=>200,
    			"detalle"=>"Se ha borrado la clase con éxito"

	    	);
    	
    		echo json_encode($json, true); 

    		return;
    	}
	}
}