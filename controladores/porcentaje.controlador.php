<?php

class ControladorPorcentaje{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index($idC, $mesI, $mesF, $anio){
	
		$lista = ModeloPorcentaje::index($idC, $mesI, $mesF, $anio);

		$json = array(

			"status"=>200,
			"total_registros"=>count($lista),
			"detalle"=>$lista
		);

		echo json_encode($json, true);

		return;
	}
}