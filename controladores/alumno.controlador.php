<?php

class ControladorAlumno{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$alumno = ModeloAlumno::index("alumno");

		$json = array(

			"status"=>200,
			"total_registros"=>count($alumno),
			"detalle"=>$alumno
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Registrar un nuevo alumno
	=============================================*/
	public function create($datos){

		/*=============================================
		Validar nombre
		=============================================*/

		if(isset($datos["nombre"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["nombre"])||
			isset($datos["apellido"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["apellido"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo nombre y apellido, solo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}

		/*=============================================
		Validar que el nombre no este repetido
		=============================================*/
		$alumno = ModeloAlumno::index("alumno");

		foreach ($alumno as $key => $value) {
			
			if($value->nombre_alumno." ".$value->apellido_alumno == $datos["nombre"]." ".$datos["apellido"]){
				$json = array(

				"status"=>404,
				"detalle"=>"El alumno ".$datos["nombre"]." ".$datos["apellido"]." ya esta registrado"

				);

				echo json_encode($json, true);

				return;		
			}
		}


		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloAlumno::create("alumno",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"El alumno se ha registrado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	
	static public function update($id, $datos){
		/*=============================================
		Validar nombre
		=============================================*/

		if(isset($datos["nombre"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["nombre"])||
			isset($datos["apellido"]) && !preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/', $datos["apellido"])){

			$json = array(

				"status"=>404,
				"detalle"=>"Error en el campo nombre y apellido, solo se permiten letras"

			);

			echo json_encode($json, true);

			return;
		}
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$edit = ModeloAlumno::update("alumno", $id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($edit == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"El alumno se ha modificado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}
	}

	public function formato($idClase, $anio){

		$alumno = ModeloAlumno::formato($idClase,$anio);

		$json = array(

			"status"=>200,
			"total_registros"=>count($alumno),
			"detalle"=>$alumno
		);

		echo json_encode($json, true);

		return;
	}

	static public function delete($valor){

		/*=============================================
		Verificar que el alumno no este matriculado
		=============================================*/
		$item = "id_alumno";
		$lista = ModeloMatricula::show("matricula", $item, $valor);

		if(count($lista) > 0){
			$json = array(

	    		"status"=>300,
	    		"total_registros"=>count($lista),
	    		"detalles"=>"El alumno esta matriculado, no puede ser borrado"
	    		
	    	);

			echo json_encode($json, true);	

			return;
		} else {

			$borrar = ModeloAlumno::delete("alumno", $valor);

			if($borrar == "ok")
				$json = array(
        	 	"status"=>200,
        	 	"total_registros"=>count($lista),
	    		"detalle"=>"Se ha borrado al alumno con éxito"

	    	);
	    	
	    	echo json_encode($json, true); 

	    	return;
		}
	}
}