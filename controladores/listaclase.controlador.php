<?php

class ControladorListaClase{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index($estado, $clase, $anio){
		if($estado == 1){
			$listaclase = ModeloListaClase::index(1, $estado, $clase, $anio);
			$listaclase2 = ModeloListaClase::index(1, 0, $clase, $anio);
			$json = array(

				"status"=>200,
				"total_alumnos"=>count($listaclase) + count($listaclase2),
				"total_registros"=>count($listaclase),
				"detalle"=>$listaclase
			);
		} else {
			$listaclase = ModeloListaClase::index(1, $estado, $clase, $anio);
			$listaclase2 = ModeloListaClase::index(1, 1, $clase, $anio);

			$json = array(

				"status"=>200,
				"total_alumnos"=>count($listaclase) + count($listaclase2),
				"total_registros"=>count($listaclase),
				"detalle"=>$listaclase
			);

		}
		echo json_encode($json, true);

		return;
	}

}