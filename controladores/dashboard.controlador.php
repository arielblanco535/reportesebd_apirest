<?php

class ControladorDashboard{

	/*=============================================
	MOSTRAR DATOS
	=============================================*/

	static public function index($mes, $anio){

		$dash = ModeloDashboard::index($mes, $anio);

		$json = array(

			"status"=>200,
			"total_registros"=>count($dash),
			"detalle"=>$dash
		);

		echo json_encode($json, true);

		return;

	}

}