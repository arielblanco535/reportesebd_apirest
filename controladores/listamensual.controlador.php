<?php

class ControladorListaMensual{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index($idC, $mes, $anio){
	
		$encuentros = ModeloListaMensual::index($idC, $mes, $anio);

		$json = array(

			"status"=>200,
			"total_registros"=>count($encuentros),
			"detalle"=>$encuentros
		);

		echo json_encode($json, true);

		return;
	}

	public function diasMes($mes, $anio){
		$dias = ModeloListaMensual::diasMes($mes, $anio);
		$json = array(

			"status"=>200,
			"total_registros"=>count($dias),
			"detalle"=>$dias
		);

		echo json_encode($json, true);
		return;
	}

}