<?php

class ControladorPromedio{

	/*=============================================
	MOSTRAR PROMEDIOS
	=============================================*/

	static public function index($anio){

		$prom = ModeloPromedio::index($anio);

		$json = array(

			"status"=>200,
			"total_registros"=>count($prom),
			"detalle"=>$prom
		);

		echo json_encode($json, true);

		return;

	}

	static public function indexTotal($anio){

		$prom = ModeloPromedio::indexTotal($anio);

		$json = array(

			"status"=>200,
			"total_registros"=>count($prom),
			"detalle"=>$prom
		);

		echo json_encode($json, true);

		return;

	}
}