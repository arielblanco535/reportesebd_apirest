<?php

class ControladorEncuentro{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$encuentros = ModeloEncuentro::index("encuentro");

		$json = array(

			"status"=>200,
			"total_registros"=>count($encuentros),
			"detalle"=>$encuentros
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Registrar un nuevo Encuentro
	=============================================*/
	public function create($datos){
		/*=============================================
		Validar que La fecha no este repetida
		=============================================*/
		$enc = ModeloEncuentro::index("encuentro");


		foreach ($enc as $key => $value) {
			
			if($value->fecha == $datos["fecha"]){
				$json = array(

				"status"=>300,
				"detalle"=>"La fecha ".$datos["fecha"]." ya esta registrado"

				);

				echo json_encode($json, true);

				return;		
			}

		}

		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloEncuentro::create("encuentro",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>400,
				"detalle"=>"El Encuentro se ha registrado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	/*=============================================
	Mostrar un solo encuentro
	=============================================*/
	public function show($id){
		/*=============================================
		Mostrar todos los encuentros
		=============================================*/
		$item = ModeloEncuentro::show("encuentro", $id);

		if(!empty($item)){

			$json = array(

				"status"=>200,
				"detalle"=>$item

			);

			echo json_encode($json, true);

			return;

		}else{

			$json = array(

	    		"status"=>300,
	    		"total_registros"=>0,
	    		"detalles"=>"No hay ningún encuentro registrado"
	    		
	    	);

			echo json_encode($json, true);	

			return;

		}
	}


	static public function update($id, $datos){
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$edit = ModeloEncuentro::update("encuentro", $id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($edit == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"El Encuentro se ha modificado",
				"consulta"=>"ok"

				);

				echo json_encode($json, true);

				return;		
		}
	}

	static public function delete($id){

		/*=============================================
		Verificar que no hay registros en ofrendas
		=============================================*/
		$item = ModeloEncuentro::show("encuentro", $id);

		foreach ($item as $key => $value) {
			
			$list = ModeloOfrenda::show("ofrenda",$value->fecha);

			if(count($list) > 0){
				$json = array(

		    		"status"=>200,
		    		"total_registros"=>count($list),
		    		"detalles"=>"Hay ofrendas registradas para este encuentro"
		    		
		    	);

				echo json_encode($json, true);	

				return;
			} else {

				$borrar = ModeloEncuentro::delete("encuentro", $id);

				if($borrar == "ok")
					$json = array(
	        	 	"status"=>200,
	        	 	"total_registros"=>count($list),
		    		"detalle"=>"Se ha borrado el encuentro con éxito"

		    	);
		    	
		    	echo json_encode($json, true); 

		    	return;
			}

		}

	}
}