<?php 

class ControladorMatricula{

	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index(){
		$matricula = ModeloMatricula::index("v_listadoalumnos");

		$json = array(

			"status"=>200,
			"total_registros"=>count($matricula),
			"detalle"=>$matricula
		);

		echo json_encode($json, true);

		return;
	}

	/*=============================================
	Mostrar un solo registro
	=============================================*/
	public function show($id){
		$matricula = ModeloMatricula::show("v_listadoalumnos", "id_matricula", $id);
		$mat = ModeloMatricula::show("matricula", "id_matricula", $id);
		$fecha = "";
		foreach ($mat as $key => $value) {
			$fecha=$value->fecha;
			break;
		}
		foreach ($matricula as $key => $value) {
				
			$json = array(

				"status"=>200,
				"total_registros"=>count($matricula),
				"matricula"=>$value->id_matricula,
				"codigo"=>$value->codigo_alumno,
				"nombre"=>$value->nombre_alumno,
				"apellido"=>$value->apellido_alumno,
				"fechaNac"=>$value->fecha_nacimiento,
				"fecha"=>$fecha,
				"clase"=>$value->id_clase,
				"estado"=>$value->estado,
				"anio"=>$value->anio
			);

			echo json_encode($json, true);

			return;
		}
	}

	/*=============================================
	Crear nueva matricula
	=============================================*/
	public function create($datos){


		/*=============================================
		Validar que el alumno no este matriculado
		=============================================*/
		$matricula = ModeloMatricula::index2("matricula");

		foreach ($matricula as $key => $value) {
			
			if($value->id_alumno == $datos["idAlumno"] && $value->id_periodo == $datos["idPeriodo"] && $value->id_clase == $datos["idClase"]){
				$json = array(

				"status"=>404,
				"detalle"=>"El alumno ya esta matricula"

				);

				echo json_encode($json, true);

				return;		
			}
		}

		/*=============================================
		Validar que el año no sea anterior al actual
		=============================================*/
		$periodo = ModeloPeriodo::index("periodo");
		foreach ($periodo as $key => $value) {
			if( $datos["idPeriodo"] == $value->id_periodo){
				if($value->anio < date('Y')){
					$json = array(

					"status"=>300,
					"detalle"=>"El año no puede ser anterior al actual"

					);

					echo json_encode($json, true);

					return;		
				}
			}
		}

		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$create = ModeloMatricula::create("matricula",$datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($create == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"La matricula se ha registrado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	/*=============================================
	Editar matricula
	=============================================*/
	public function update($id, $datos){
		/*=============================================
		Validar que el año no sea anterior al actual
		=============================================*/
		$periodo = ModeloPeriodo::index("periodo");
		foreach ($periodo as $key => $value) {
			if( $datos["periodo"] == $value->id_periodo){
				if($value->anio < date('Y')){
					$json = array(

					"status"=>300,
					"detalle"=>"El año no puede ser anterior al actual"

					);

					echo json_encode($json, true);

					return;		
				}
			}
		}

		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$update = ModeloMatricula::update("matricula", $id, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if($update == "ok"){
			$json = array(

				"status"=>200,
				"detalle"=>"La matricula se ha modificado"

				);

				echo json_encode($json, true);

				return;		
		}

	}

	public function actualizar($datos){
		/*=============================================
		Llevar datos al modelo
		=============================================*/
		$respuesta = ModeloMatricula::actualizar("matricula", $datos["item1"], $datos["valor1"], $datos["item2"], $datos["valor2"]);

		/*=============================================
		Respuesta del modelo
		=============================================*/
		if($respuesta == "ok"){
			$json = array(

				"status"=>200,
				"result"=>"ok",
				"detalle"=>"La matricula ha sido actualizada"

				);

			echo json_encode($json, true);

			return;		
		}
	}
}
