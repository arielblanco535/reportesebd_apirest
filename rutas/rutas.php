<?php

$arrayRutas = explode("/", $_SERVER['REQUEST_URI']);

if(isset($_GET["page"]) && is_numeric($_GET["page"])){	

	$cursos = new ControladorCursos();
	$cursos -> index($_GET["page"]);	

}else{

	if(count(array_filter($arrayRutas)) == 0){
		/*=============================================
		Cuando no se hace ninguna petición a la API
		=============================================*/
				
		$json = array(

		"detalle"=>"no encontrado index"

		);

		echo json_encode($json, true);

		return;
	}else{
		/*=============================================
		Cuando pasamos solo un índice en el array $arrayRutas
		=============================================*/

		if(count(array_filter($arrayRutas)) == 1){	

			/*=============================================
			Cuando se hace peticiones desde registro
			=============================================*/

			if(array_filter($arrayRutas)[1] == "clases"){

				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){

					$clases = new ControladorClase();
					$clases -> index();	

				}

				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

					/*=============================================
					Capturando datos
					=============================================*/
					$datos = array("nombre"=>$_POST["nombre"]);
	
					$clases = new ControladorClase();
					$clases -> create($datos);

				}else{

					$json = array(

						"detalle"=>"detalle no encontrado"

					);

					echo json_encode($json, true);

					return;

				}
			}
				
			else if(array_filter($arrayRutas)[1] == "maestros"){

				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){

					$maestros = new ControladorMaestro();
					$maestros -> index();

				}

				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

					/*=============================================
					Capturando datos
					=============================================*/
					$datos = array("nombre"=>$_POST["nombre"],
									"apellido"=>$_POST["apellido"],
									"fecha_nacimiento"=>$_POST["fecha_nacimiento"],
									"cedula"=>$_POST["cedula"],
									"id_clase"=>$_POST["id_clase"]);

					$maestros = new ControladorMaestro();
					$maestros -> create($datos);

				}else{

					$json = array(

						"detalle"=>"detalle no encontrado"

					);

					echo json_encode($json, true);

					return;

				}
			} else if(array_filter($arrayRutas)[1] == "encuentros"){
				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
					$encuentro = new ControladorEncuentro();
					$encuentro -> index();

				}
				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

					/*=============================================
					Capturando datos
					=============================================*/
					$datos = array("fecha"=>$_POST["fecha"],
									"observacion"=>$_POST["observacion"]);

					$enc = new ControladorEncuentro();
					$enc -> create($datos);
				}
			} else if(array_filter($arrayRutas)[1] == "ofrendas"){
				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
					$encuentro = new ControladorOfrenda();
					$encuentro -> index();

				}
				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

					/*=============================================
					Capturando datos
					=============================================*/
					$datos = array("monto"=>$_POST["monto"],
									"fecha"=>$_POST["fecha"],
									"idClase"=>$_POST["idClase"]);

					$crear = new ControladorOfrenda();
					$crear -> create($datos);
				}
			} else if(array_filter($arrayRutas)[1] == "periodos"){
				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
					$periodo = new ControladorPeriodo();
					$periodo -> index();

				}
				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

					/*=============================================
					Capturando datos
					=============================================*/
					$datos = array("anio"=>$_POST["anio"],
									"fecha"=>$_POST["fecha"]);

					$crear = new ControladorPeriodo();
					$crear -> create($datos);
				}			
			} else if(array_filter($arrayRutas)[1] == "usuarios"){
				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
					$user = new ControladorUsuario();
					$user -> index();

				}

				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					if($_POST["tipo"] == 1){
						$item1 = $_POST["item1"];
						$valor1 = $_POST["valor1"];
						$item2 = $_POST["item2"];
						$valor2 = $_POST["valor2"];

						$actualizar = new ControladorUsuario();
						$actualizar -> actualizar($item1, $valor1, $item2, $valor2);

					}

					else if($_POST["tipo"] == 2){
						$datos = array("nombre"=>$_POST["nombre"],
										"usuario"=>$_POST["usuario"],
										"password"=>$_POST["password"],
										"perfil"=>$_POST["perfil"],
										"foto"=>$_POST["ruta"]);
						$user = new ControladorUsuario();
						$user -> create($datos);
					}
				}else{

					$json = array(

						"detalle"=>"detalle no encontrado"

					);

					echo json_encode($json, true);

					return;
				}
			} else if(array_filter($arrayRutas)[1] == "listaclase"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$estado = $_POST["estado"];
					$clase = $_POST["clase"];
					$anio = $_POST["anio"];
					$listaclase = new ControladorListaClase();
					$listaclase -> index($estado, $clase, $anio);

				}
			}else if(array_filter($arrayRutas)[1] == "listamensual"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$idC = $_POST["idc"];
					$mes = $_POST["mes"];
					$anio = $_POST["anio"];
					$listamensual = new ControladorListaMensual();
					$listamensual -> index($idC, $mes, $anio);
				}
			} else if(array_filter($arrayRutas)[1] == "dominical"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$clase = $_POST["clase"];
					$fecha = $_POST["fecha"];
					$dominical = new ControladorADominical();
					$dominical -> index($clase, $fecha);
				}
			} else if(array_filter($arrayRutas)[1] == "diasmes"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$mes = $_POST["mes"];
					$anio = $_POST["anio"];
					$dias = new ControladorListaMensual();
					$dias -> diasMes($mes, $anio);
				}
			} else if(array_filter($arrayRutas)[1] == "porcentaje"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$idC = $_POST["idC"];
					$mesI = $_POST["mesI"];
					$mesF = $_POST["mesF"];
					$anio = $_POST["anio"];
					$dias = new ControladorPorcentaje();
					$dias -> index($idC, $mesI, $mesF, $anio);
				}
			} else if(array_filter($arrayRutas)[1] == "dashboard"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$mes = $_POST["mes"];
					$anio = $_POST["anio"];
					$dias = new ControladorDashboard();
					$dias -> index($mes, $anio);
				}
			} else if(array_filter($arrayRutas)[1] == "promediomensual"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$anio = $_POST["anio"];
					$dias = new ControladorPromedio();
					$dias -> index($anio);
				}
			} else if(array_filter($arrayRutas)[1] == "promediototal"){
				/*=============================================
				Peticiones POST
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$anio = $_POST["anio"];
					$dias = new ControladorPromedio();
					$dias -> indexTotal($anio);
				}
			} else if(array_filter($arrayRutas)[1] == "alumno"){

				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
					$Alumno = new ControladorAlumno();
					$Alumno -> index();
				}
				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$datos = array("codigo"=>$_POST["codigo"],
									"nombre"=>$_POST["nombre"],
									"apellido"=>$_POST["apellido"],
									"fechaNac"=>$_POST["fechaNac"],
									"fechaIng"=>$_POST["fechaIng"]);

					$Alumno = new ControladorAlumno();
					$Alumno -> create($datos);
				}
			} else if(array_filter($arrayRutas)[1] == "matricula"){

				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
					$matricula = new ControladorMatricula();
					$matricula -> index();
				}
				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$datos = array("idClase"=>$_POST["clase"],
									"idAlumno"=>$_POST["alumno"],
									"idPeriodo"=>$_POST["periodo"],
									"fecha"=>$_POST["fecha"],
									"estado"=>$_POST["estado"]);

					$matricula = new ControladorMatricula();
					$matricula -> create($datos);
				}
			} else if(array_filter($arrayRutas)[1] == "asistencia"){
				/*=============================================
				Peticiones GET
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
					$asistencia = new ControladorAsistencia();
					$asistencia -> index();
				}
				/*=============================================
				Peticiones POST
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$datos = array("encuentro"=>$_POST["idEncuentro"],
									"matricula"=>$_POST["idMatricula"],
									"estado"=>$_POST["estado"]);

					$asistencia = new ControladorAsistencia();
					$asistencia -> create($datos);
				}
			}
		}else{
		
			/*=============================================
			Cuando se hacen Peticiones para editar, elminar
			=============================================*/

			if(array_filter($arrayRutas)[1] == "encuentros" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones GET Encuentro
				=============================================*/

				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
				
					$enc = new ControladorEncuentro();
					$enc -> show(array_filter($arrayRutas)[2]);
				}

				/*=============================================
				Peticiones PUT Encuentro
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){

					/*=============================================
					Capturando datos para editar
					=============================================*/
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$editar = new ControladorEncuentro();
					$editar -> update(array_filter($arrayRutas)[2], $datos);
				}
				/*=============================================
				Peticiones DELETE
				=============================================*/

				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE"){

					$borrar = new ControladorEncuentro();
					$borrar -> delete(array_filter($arrayRutas)[2]);	

				}else{

					$json = array(

						"detalle"=>"no encontrado"

					);

					echo json_encode($json, true);

					return;
				
				}

			}
			else if(array_filter($arrayRutas)[1] == "ofrendas" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones GET Encuentro
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
				
					$index = new ControladorOfrenda();
					$index -> show(array_filter($arrayRutas)[2]);
				}


				/*=============================================
				Peticiones PUT Ofrenda
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){

					/*=============================================
					Capturando datos para editar
					=============================================*/
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$editar = new ControladorOfrenda();
					$editar -> update(array_filter($arrayRutas)[2], $datos);
				}
				/*=============================================
				Peticiones DELETE
				=============================================*/

				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE"){

					$borrar = new ControladorOfrenda();
					$borrar -> delete(array_filter($arrayRutas)[2]);	

				}else{

					$json = array(

						"detalle"=>"no encontrado"

					);

					echo json_encode($json, true);

					return;
				
				}

			}
			/*=============================================
			PETICIONES ALUMNO
			=============================================*/
			else if(array_filter($arrayRutas)[1] == "alumno" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones POST Alumno
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$anio = $_POST["anio"];
					$Alumno = new ControladorAlumno();
					$Alumno -> formato(array_filter($arrayRutas)[2], $anio);
				}
				/*=============================================
				Peticiones PUT Alumno
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$Alumno = new ControladorAlumno();
					$Alumno -> update(array_filter($arrayRutas)[2], $datos);
				}
				/*=============================================
				Peticiones DELETE Alumno
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE"){
					$Alumno = new ControladorAlumno();
					$Alumno -> delete(array_filter($arrayRutas)[2]);
				}
			}
			/*=============================================
			PETICIONES MAESTRO
			=============================================*/
			else if(array_filter($arrayRutas)[1] == "maestros" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones POST maestro
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$datos = array("item1"=>$_POST["item1"],
									"valor1"=>$_POST["valor1"],
									"item2"=>$_POST["item2"],
									"valor2"=>$_POST["valor2"],
									"idClase"=>array_filter($arrayRutas)[2]);

					$maestro = new ControladorMaestro();
					$maestro -> actualizar($datos);
				}
				/*=============================================
				Peticiones PUT Maestro
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$maestro = new ControladorMaestro();
					$maestro -> update(array_filter($arrayRutas)[2], $datos);
				}
				/*=============================================
				Peticiones DELETE Maestro
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE"){
					$maestro = new ControladorMaestro();
					$maestro -> delete(array_filter($arrayRutas)[2]);
				}
			}
			/*=============================================
			PETICIONES MATRICULA
			=============================================*/
			else if(array_filter($arrayRutas)[1] == "matricula" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones GET Matricula
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
				
					$show = new ControladorMatricula();
					$show -> show(array_filter($arrayRutas)[2]);
				}
				/*=============================================
				Peticiones POST maestro
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
					$datos = array("item1"=>$_POST["item1"],
									"valor1"=>$_POST["valor1"],
									"item2"=>$_POST["item2"],
									"valor2"=>$_POST["valor2"],
									"idMatricula"=>array_filter($arrayRutas)[2]);

					$act = new ControladorMatricula();
					$act -> actualizar($datos);
				}
				/*=============================================
				Peticiones PUT Matricula
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$maestro = new ControladorMatricula();
					$maestro -> update(array_filter($arrayRutas)[2], $datos);
				}
				/*=============================================
				Peticiones DELETE Maestro
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE"){
					$maestro = new ControladorMaestro();
					$maestro -> delete(array_filter($arrayRutas)[2]);
				}
			}			
			/*=============================================
			PETICIONES ASISTENCIA
			=============================================*/
			else if(array_filter($arrayRutas)[1] == "asistencia" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones GET Asistencia
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET"){
				
					$show = new ControladorMatricula();
					$show -> show(array_filter($arrayRutas)[2]);
				}
				/*=============================================
				Peticiones PUT Maestro
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$update = new ControladorAsistencia();
					$update -> update(array_filter($arrayRutas)[2], $datos);
				}
			}
			/*=============================================
			PETICIONES CLASE
			=============================================*/
			else if(array_filter($arrayRutas)[1] == "clases" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones PUT Clase
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$update = new ControladorClase();
					$update -> update(array_filter($arrayRutas)[2], $datos);
				}
				/*=============================================
				Peticiones DELETE Clase
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE"){
					$delete = new ControladorClase();
					$delete -> delete(array_filter($arrayRutas)[2]);
				}
			}
			/*=============================================
			PETICIONES PERIODO
			=============================================*/
			else if(array_filter($arrayRutas)[1] == "periodos" && is_numeric(array_filter($arrayRutas)[2])){
				/*=============================================
				Peticiones PUT Periodo
				=============================================*/
				if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT"){
					$datos = array();

					parse_str(file_get_contents('php://input'), $datos);	

					$update = new ControladorPeriodo();
					$update -> update(array_filter($arrayRutas)[2], $datos);
				}
				/*=============================================
				Peticiones DELETE Periodo
				=============================================*/
				else if(isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE"){
					$delete = new ControladorPeriodo();
					$delete -> delete(array_filter($arrayRutas)[2]);
				}
			}
		}
	}
}