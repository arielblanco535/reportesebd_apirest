<?php

require_once "controladores/rutas.controlador.php";
require_once "controladores/clase.controlador.php";
require_once "controladores/maestro.controlador.php";
require_once "controladores/listaclase.controlador.php";
require_once "controladores/periodo.controlador.php";
require_once "controladores/adominical.controlador.php";
require_once "controladores/encuentro.controlador.php";
require_once "controladores/ofrenda.controlador.php";
require_once "controladores/listamensual.controlador.php";
require_once "controladores/usuario.controlador.php";
require_once "controladores/porcentaje.controlador.php";
require_once "controladores/dashboard.controlador.php";
require_once "controladores/promedio.controlador.php";
require_once "controladores/alumno.controlador.php";
require_once "controladores/matricula.controlador.php";
require_once "controladores/asistencia.controlador.php";

require_once "modelos/clase.modelo.php";
require_once "modelos/maestro.modelo.php";
require_once "modelos/listaclase.modelo.php";
require_once "modelos/periodo.modelo.php";
require_once "modelos/adominical.modelo.php";
require_once "modelos/encuentro.modelo.php";
require_once "modelos/ofrenda.modelo.php";
require_once "modelos/listamensual.modelo.php";
require_once "modelos/usuario.modelo.php";
require_once "modelos/porcentaje.modelo.php";
require_once "modelos/dashboard.modelo.php";
require_once "modelos/promedio.modelo.php";
require_once "modelos/alumno.modelo.php";
require_once "modelos/matricula.modelo.php";
require_once "modelos/asistencia.modelo.php";

$rutas = new ControladorRutas();
$rutas -> index();