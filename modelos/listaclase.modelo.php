<?php

require_once "conexion.php";

class ModeloListaClase{

	static public function index($i, $estado, $clase, $anio){
		if($i == 1){
			$stmt = Conexion::conectar()->prepare("call SP_listaClase(".$estado.",".$clase.",".$anio.")");

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_CLASS);
		}

		$stmt -> close();

		$stmt = null;
	}
}