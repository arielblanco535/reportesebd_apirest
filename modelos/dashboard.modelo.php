<?php

	require_once "conexion.php";

class ModeloDashboard{

	/*=============================================
	MOSTRAR LISTA
	=============================================*/
	
	static public function index($mes, $anio){

		$stmt = Conexion::conectar()->prepare("call SP_dashboard(".$mes.",".$anio.")");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);			

		$stmt -> close();

		$stmt = null;

	}	

}
