<?php

require_once "conexion.php";

class ModeloMatricula{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index($tabla){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY nombre_alumno");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	public function index2($tabla){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	/*=============================================
	Mostrar una sola alumno matricula
	=============================================*/
	static public function show($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item =:id");

		$stmt -> bindParam(":id", $valor, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(id_clase, id_alumno, id_periodo, fecha, estado) VALUES(:clase, :alumno, :periodo, :fecha, :estado)");

		$stmt -> bindParam(":clase", $datos["idClase"], PDO::PARAM_STR);
		$stmt -> bindParam(":alumno", $datos["idAlumno"], PDO::PARAM_STR);
		$stmt -> bindParam(":periodo", $datos["idPeriodo"], PDO::PARAM_STR);
		$stmt -> bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt -> bindParam(":estado", $datos["estado"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	static public function update($tabla, $id, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET id_clase = :clase, id_alumno = :alumno, id_periodo = :periodo, fecha = :fecha, estado = :estado WHERE id_matricula = :matricula");

		$stmt -> bindParam(":clase", $datos["clase"], PDO::PARAM_STR);
		$stmt -> bindParam(":alumno", $datos["alumno"], PDO::PARAM_STR);
		$stmt -> bindParam(":periodo", $datos["periodo"], PDO::PARAM_STR);
		$stmt -> bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt -> bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
		$stmt -> bindParam(":matricula", $id, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	public function actualizar($tabla, $item1, $valor1, $item2, $valor2){

		if($item1 != ""){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

			$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

			if($stmt -> execute()){

				return "ok";
		
			}else{

				return "error";	

			}

			$stmt -> close();

			$stmt = null;
		}

	}
}