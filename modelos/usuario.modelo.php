 <?php

require_once "conexion.php";

class ModeloUsuario{

	static public function index($i,$tablas){
		if($i == 1){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas ORDER BY nombre");

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_CLASS);
		}

		if($i == 2){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas");

			$stmt -> execute();

			return $stmt -> fetchAll();			
		}
		$stmt -> close();

		$stmt = null;
	}

	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(nombre, usuario, password, perfil, foto) VALUES (:nombre, :usuario, :password, :perfil, :foto)");

		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $datos["password"], PDO::PARAM_STR);
		$stmt->bindParam(":perfil", $datos["perfil"], PDO::PARAM_STR);
		$stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	public function actualizar($tabla, $item1, $valor1, $item2, $valor2){

		if($item1 != ""){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

			$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

			if($stmt -> execute()){

				return "ok";
		
			}else{

				return "error";	

			}

			$stmt -> close();

			$stmt = null;
		}

	}
}