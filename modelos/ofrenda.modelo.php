<?php

require_once "conexion.php";

class ModeloOfrenda{

	/*=============================================
	Mostrar todos los registros
	=============================================*/
	static public function index($tablas){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	Mostrar fechas
	=============================================*/
	static public function show($tabla, $fecha){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha_recaudado=:fecha");

		$stmt -> bindParam(":fecha", $fecha, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	/*=============================================
	Mostrar por clase
	=============================================*/
	static public function showClase($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_clase=:id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	/*=============================================
	Crear Ofrenda
	=============================================*/
	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(monto, fecha_recaudado, id_clase) VALUES (:monto, :fecha, :idClase)");

		$stmt->bindParam(":monto", $datos["monto"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt->bindParam(":idClase", $datos["idClase"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Editar Ofrenda
	=============================================*/
	static public function update($tabla, $id, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET monto = :monto, fecha_recaudado = :fecha, id_clase = :idClase WHERE id_ofrenda = :idOfrenda");

		$stmt->bindParam(":monto", $datos["monto"], PDO::PARAM_STR);
		$stmt -> bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt -> bindParam(":idClase", $datos["idClase"], PDO::PARAM_STR);
		$stmt -> bindParam(":idOfrenda", $id, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Borrar Ofrenda
	=============================================*/
	static public function delete($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_ofrenda = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt-> close();

		$stmt = null;

	}	

}