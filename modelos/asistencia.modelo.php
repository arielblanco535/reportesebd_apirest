<?php

require_once "conexion.php";

class ModeloAsistencia{


	/*=============================================
	Mostrar todos los registros
	=============================================*/
	public function index($tabla){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	/*=============================================
	Mostrar una sola alumno matricula
	=============================================*/
	static public function show($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item =:id");

		$stmt -> bindParam(":id", $valor, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(id_encuentro, id_matricula, presente) VALUES(:encuentro, :matricula, :estado)");

		$stmt -> bindParam(":encuentro", $datos["encuentro"], PDO::PARAM_STR);
		$stmt -> bindParam(":matricula", $datos["matricula"], PDO::PARAM_STR);
		$stmt -> bindParam(":estado", $datos["estado"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	static public function update($tabla, $id, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET id_encuentro = :encuentro, id_matricula = :matricula, presente = :presente WHERE id_asistencia = :idAsistencia");

		$stmt -> bindParam(":encuentro", $datos["encuentro"], PDO::PARAM_STR);
		$stmt -> bindParam(":matricula", $datos["matricula"], PDO::PARAM_STR);
		$stmt -> bindParam(":presente", $datos["presente"], PDO::PARAM_STR);
		$stmt -> bindParam(":idAsistencia", $id, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;
	}


}