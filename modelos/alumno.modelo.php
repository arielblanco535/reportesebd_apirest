<?php

require_once "conexion.php";

class ModeloAlumno{

	static public function index($tablas){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas ORDER BY nombre_alumno");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

		$stmt -> close();

		$stmt = null;
	}

	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(codigo_alumno, nombre_alumno, apellido_alumno, fecha_nacimiento, fecha_ingreso) VALUES(:codigo, :nombre, :apellido, :fechaNac, :fechaIng)");

		$stmt -> bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datos["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":fechaNac", $datos["fechaNac"], PDO::PARAM_STR);
		$stmt -> bindParam(":fechaIng", $datos["fechaIng"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	static public function update($tabla, $id, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre_alumno = :nombre, apellido_alumno = :apellido, fecha_nacimiento = :fechaNac, fecha_ingreso = :fechaIng WHERE id_alumno = :idAlumno");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datos["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":fechaNac", $datos["fechaNac"], PDO::PARAM_STR);
		$stmt -> bindParam(":fechaIng", $datos["fechaIng"], PDO::PARAM_STR);
		$stmt -> bindParam(":idAlumno", $id, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;
	}

	static public function formato($idClase, $anio){
		$stmt = Conexion::conectar()->prepare("call SP_ListaDominical(".$idClase.",1,".$anio.")");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);			

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Borrar Alumno
	=============================================*/
	static public function delete($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_alumno = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt-> close();

		$stmt = null;

	}	
}