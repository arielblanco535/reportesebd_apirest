<?php

require_once "conexion.php";

class ModeloListaMensual{

	static public function index($idC, $mes, $anio){

		$stmt2 = Conexion::conectar()->prepare("call SP_diasdelmes(".$anio.",".$mes.")");

		$stmt2 -> execute();

		$lista = $stmt2 -> fetchAll();

		$fecha1 = "2000-01-01";		$fecha2 = "2000-01-01";		$fecha3 = "2000-01-01";		$fecha4 = "2000-01-01";		$fecha5 = "2000-01-01";
		foreach ($lista as $key => $value) {
			if($fecha1 == "2000-01-01" && $key <= count($lista))
				$fecha1 = $value["fecha"];
			else if($fecha2 == "2000-01-01" && $key <= count($lista))
				$fecha2 = $value["fecha"];
			else if($fecha3 == "2000-01-01" && $key <= count($lista))
				$fecha3 = $value["fecha"];
			else if($fecha4 == "2000-01-01" && $key <= count($lista))
				$fecha4 = $value["fecha"];
			else if($fecha5 == "2000-01-01" && $key <= count($lista))
				$fecha5 = $value["fecha"];			
		}

		$stmt = Conexion::conectar()->prepare("call SP_asistenciamensual('".$fecha1."','".$fecha2."',".$idC.",'".$fecha3."','".$fecha4."','".$fecha5."',".$mes.", ".$anio.")");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

		$stmt2 -> close();

		$stmt2 = null;

		$stmt -> close();

		$stmt = null;

	}

	public static function diasMes($mes, $anio){

		$stmt2 = Conexion::conectar()->prepare("call SP_diasdelmes(".$anio.",".$mes.")");

		$stmt2 -> execute();

		return $stmt2 -> fetchAll(PDO::FETCH_CLASS);

		$stmt2 -> close();

		$stmt2 = null;

	}

}