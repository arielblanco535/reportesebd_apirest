<?php

require_once "conexion.php";

class ModeloMaestro{

	static public function index($tablas){
		
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas ORDER BY nombre_maestro");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

		$stmt -> close();

		$stmt = null;
	}

	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(nombre_maestro, apellido_maestro, fecha_nacimiento, cedula, id_clase) VALUES(:nombre, :apellido, :fecha, :cedula, :id)");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datos["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":fecha", $datos["fecha_nacimiento"], PDO::PARAM_STR);
		$stmt -> bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datos["id_clase"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Mostrar un solo maestro
	=============================================*/
	static public function show($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_maestro=:id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	public function actualizar($tabla, $item1, $valor1, $item2, $valor2){

		if($item1 != ""){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

			$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

			if($stmt -> execute()){

				return "ok";
		
			}else{

				return "error";	

			}

			$stmt -> close();

			$stmt = null;
		}

	}

	public function update($tabla, $id, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre_maestro = :nombre, apellido_maestro = :apellido, fecha_nacimiento = :fecha, cedula = :cedula, id_clase = :idClase WHERE id_maestro = :idMaestro");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datos["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt -> bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
		$stmt -> bindParam(":idClase", $datos["idClase"], PDO::PARAM_STR);
		$stmt -> bindParam(":idMaestro", $id, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}
	/*=============================================
	Borrar Maestro
	=============================================*/
	static public function delete($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_maestro = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt-> close();

		$stmt = null;

	}	
}