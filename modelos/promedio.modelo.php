<?php

require_once "conexion.php";

class ModeloPromedio{

	static public function index($anio){

		$stmt = Conexion::conectar()->prepare("call SP_promediomensual(".$anio.")");

		$stmt -> execute();
		
		return $stmt -> fetchAll(PDO::FETCH_CLASS);			

		$stmt -> close();

		$stmt = null;
	}

	static public function indexTotal($anio){

		$stmt = Conexion::conectar()->prepare("call SP_promediototal(".$anio.")");

		$stmt -> execute();
		
		return $stmt -> fetchAll(PDO::FETCH_CLASS);			

		$stmt -> close();

		$stmt = null;
	}
}