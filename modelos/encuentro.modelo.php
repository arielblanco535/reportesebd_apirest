<?php

require_once "conexion.php";

class ModeloEncuentro{

	static public function index($tablas){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas");

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

		$stmt -> close();

		$stmt = null;
	}

	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(fecha, observacion) VALUES (:fecha, :observacion)");

		$stmt->bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt->bindParam(":observacion", $datos["observacion"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Mostrar un solo encuentro
	=============================================*/
	static public function show($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_encuentro=:id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_CLASS);

	    $stmt -> close();

	    $stmt -= null;

	}

	static public function update($tabla, $id, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET fecha = :fecha, observacion = :observacion WHERE id_encuentro = :id_encuentro");

		$stmt -> bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt -> bindParam(":observacion", $datos["observacion"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_encuentro", $id, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Borrar Encuentro
	=============================================*/
	static public function delete($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_encuentro = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt-> close();

		$stmt = null;

	}	

}