<?php

require_once "conexion.php";

class ModeloClase{

	static public function index($i,$tablas){
		if($i == 1){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas ORDER BY nombre");

			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_CLASS);
		}

		if($i == 2){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tablas");

			$stmt -> execute();

			return $stmt -> fetchAll();			
		}
		$stmt -> close();

		$stmt = null;
	}

	static public function create($tablas, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tablas(nombre) VALUES(:nombre)");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		} else {

			print_r(Conexion::conectar()->errorInfo());
		
		}
		
		$stmt -> close();

		$stmt = null;

	}

	static public function update($tabla, $id, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre WHERE id_clase = :id_clase");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_clase", $id, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Borrar Clase
	=============================================*/
	static public function delete($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_clase = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt-> close();

		$stmt = null;

	}	
}